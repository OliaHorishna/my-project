﻿#include <iostream>
#include <string>
using namespace std;

void do_summa(float num1, float num2, float& result)
{
	result = num1 + num2;
	cout << "Summa chisel " << num1 << " i " << num2 << " ravna " << result << endl;
}

void do_raznitsa(float num1, float num2, float& result)
{
	result = num1 - num2;
	cout << "Raznitsya chisel " << num1 << " i " << num2 << " ravna " << result << endl;
}


void do_umnozhenie(float num1, float num2, float& result)
{
	result = num1 * num2;
	cout << "Umnozhenie chisel " << num1 << " i " << num2 << " ravno " << result << endl;
}

void do_delenie(float num1, float num2, float& result)
{
	result = num1 / num2;
	cout << "Delenie chisel " << num1 << " i " << num2 << " ravno " << result << endl;
}

bool isNumber(const string& str)
{
	for (char const& c : str) {
		if (std::isdigit(c) == 0) return false;
	}
	return true;
}

int main()
{
	int selection;
	float result;
	bool secnumis0 = false;
	string str1, str2;
	cout << "Vvedite pervoe chislo\n";
	cin >> str1;
	cout << "Vvedite vtoroe chislo\n";
	cin >> str2;
	
	if (isNumber(str1) && isNumber(str2)) {
		cout << "\n" << "Chto delaem?\n" << "1. Summa\n" << "2. Raznitsa\n" << "3. Umnozhenie\n" << "4. Delenie\n" << endl;
		cout << "Viberite ot 1 do 4\n" << endl;
		cin >> selection;
		cout << "\n";
		const float num1 = stof(str1);
		const float num2 = stof(str2);
		if (num2 == 0)
		{
			secnumis0 = true;
		}
		switch (selection)
		{
		case 1:
			do_summa(num1, num2, result);
			break;
		case 2:
			do_raznitsa(num1, num2, result);
			break;
		case 3:
			do_umnozhenie(num1, num2, result);
			break;
		case 4:
			if (secnumis0 == false) {


				do_delenie(num1, num2, result);
				break;
			}
			else {
				cout << "Na nol delit nelza";
				break;
			}
		
		}
		

	}
	else {
		cout << "Invalid num1 or num2" << endl;
	}
	return 0;
}