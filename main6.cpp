#include "bitwise_operations.h"

int main() {
	setlocale(LC_ALL, "Russian");
	int num = 7, pos = 2;
	cout << "Preset bit:" << returnBit(num, pos) << endl;

	cout << "Bit on:  " << bit_on(num, pos) << endl;
	cout << "Bit off: " << bit_off(num, pos) << endl;

	int a = 10, b = 7;

	cout << endl << a << '\t' << b << endl;
	bit_swap(a, b);
	cout << "Swap:\n";
	cout << a << '\t' << b << endl;


	string bin = "";
	cout << endl << "Decimal to binary:\t" << dec_to_bin(num, bin) << endl;
	cout << "Binary to decimal:\t" << bin_to_dec(bin) << endl;

	string s = "� ����� ����������������";
	cout << endl << "Message:\n" << s << endl;
	string es = encode_caesar(5, s);
	cout << "Encoded message:\n" << es << endl;
	string des = decode_caesar(5, es);
	cout << "Decoded message:\n" << des << endl;
	getchar();
	return 0;
}