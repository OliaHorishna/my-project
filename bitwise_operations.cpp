#include "bitwise_operations.h"

int returnBit(int num, int pos)
{
	return ((num >> pos) & 1);
}

long long bit_on(int num, int pos)
{

	return(num | (1 << pos));
}
long long bit_off(int num, int pos)
{
	return (num & (~(1 << pos)));
}

void bit_swap(int& a, int& b)
{
	int t = a ^ b;
	a = a ^ t;
	b = b ^ t;


}

string dec_to_bin(unsigned int num, string& bin)
{
	while (num)
	{
		bin = to_string(num & 1) + bin;
		num >>= 1;
	}
	return bin;
}



long long bin_to_dec(string bin)
{
	int dec = 0;
	for (int i = 0; i < bin.size(); i++) {
		dec <<= 1;
		dec += bin[i] - '0';
	}
	return dec;
}


string alphabet = "�����Ũ���������������������������������������������������������� .,:;-!?";

string encode_caesar(int shift, string source)
{

	string result = "";
	for (size_t i = 0; i < source.size(); ++i) {
		auto c = source[i];
		auto pos = alphabet.find(c);
		if (pos == string::npos)
		{
			continue;
		}
		auto new_pos = (pos + shift) % alphabet.size();
		result += alphabet[new_pos];
	}
	return result;
}

string decode_caesar(int shift, string source)
{
	string result = "";
	for (size_t i = 0; i < source.size(); ++i) {
		auto c = source[i];
		auto pos = alphabet.find(c);
		if (pos == string::npos)
		{
			continue;
		}
		auto new_pos = (pos - shift) % alphabet.size();
		result += alphabet[new_pos];
	}
	return result;
}

