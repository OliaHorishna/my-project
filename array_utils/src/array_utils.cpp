#include "array_utils.h"

double sum(std::vector <double>& v)
{
	double sum = 0;
	for (int i = 0; i < v.size(); i++) {
		sum += v[i];
	}
	return sum;
}

double avg(std::vector <double>& v)
{
	return sum(v) / v.size();
}