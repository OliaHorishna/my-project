#include "Simple_project.h"

double calc::calculate(const double& x, const double& a, const double& b) {

	auto result = b + x * atan(x) - log(sqrt(1 + pow(x, 2)));
	return result;
}

bool calc::calculate(const double& x, const double& a, const double& b, double& result)
{
	try {
		result = b + x * atan(x) - log(sqrt(1 + pow(x, 2)));
		return true;
	}
	catch (...) {
		return false;
	}
}

calc::Calculator::Calculator(const double& a, const double& b)
{
	this->a = a;
	this->b = b;
	this->success = false;
}

calc::Calculator::~Calculator()
{
}

const double calc::Calculator::calculate(const double& x) const
{
	try {
		auto result = calc::calculate(x, this->a, this->b);
		return result;
	}
	catch (std::exception ex) {
		throw ex;
	}
	return calc::calculate(x, this->a, this->b);
}

double calc::Calculator::operator()(const double& x) const
{
	return this->calculate (x);
}
