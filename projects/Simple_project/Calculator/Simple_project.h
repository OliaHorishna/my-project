#pragma once

#include <cmath>
#include <iostream>
#include <exception>

namespace calc {

	class Calculator
	{
	private:
		double a;
		double b;
		bool success;
	public:
		Calculator(const double& a, const double& b);
		virtual ~Calculator();
		const double calculate(const double&) const;

		double operator() (const double&) const;
	};

	double calculate(const double& x, const double& a, const double& b);

	bool calculate(const double& x, const double& a, const double& b, double& result);
}