#include "Simple_project.h"

using namespace std;

int main(int argc, char* argv[]) {

	double a = 20.1, b = -0.13, x = 0.19;

	auto result = calc::calculate(x, a, b);
	
	cout << "Result 1 is:" << result << endl;

	if (calc::calculate(x, a, b, result)) {
		cout << "Result 2 is:" << result << endl;
	}
	else {
		cout << "Error" << endl;
	}

	calc::Calculator calculator(a, b);
	cout << "Result 3 is:" << calculator.calculate(x) << endl;
	cout << "Result (x) is:" << calculator(x) << endl;
}