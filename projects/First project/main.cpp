#include <stdio.h>
#include <math.h>
#define FUNC(x) cos(pow((x), 10.0/3))
#define Q(x, n) -pow((x), 2.0/3.0)/2/(n+10)/(22*n+5)
int main() {
	double x = 2.5, S = 0.0, a = 5.0;
	unsigned int n, N = 5;
	for (n = 0; n < N; ++n) {
		S += a;
		a *= Q(x, n);
	}
	double y = FUNC(x), tol = fabs(S - y);
	printf("Sum:\t\t%.7f\nControl:\t%.7f\nTolerance:\t%.7f\n",
		S, y, tol);
	return 0;
}