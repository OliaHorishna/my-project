#include "pch.h"
#include "CppUnitTest.h"
#include "../Some massive/array_utils.h"
#include "../Some massive//array_utils.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std;

namespace UnitTest1a
{
	TEST_CLASS(UnitTest1a)
	{
	public:

		TEST_METHOD(TestSum1)
		{
			vector<double> v1{ -1, 2, 3, 24, 69 };
			auto expect_result = 97.0;
			auto actual_result = sum(v1);
			Assert::AreEqual(expect_result, actual_result);
		}

		TEST_METHOD(TestSum2)
		{
			vector<double> v1{ 5, -10, 25, 228, 1 };
			auto expect_result = 249.0;
			auto actual_result = sum(v1);
			Assert::AreEqual(expect_result, actual_result);
		}
		TEST_METHOD(TestAvg1)
		{
			vector<double> v2{ 5, 23, 7, 10, 41 };
			auto expect_result = 17.2;
			auto actual_result = avg(v2);
			Assert::AreEqual(expect_result, actual_result);
		}

		TEST_METHOD(TestAvg2)
		{
			vector<double> v2{ 47, 23, 1, 69, 41 };
			auto expect_result = 36.2;
			auto actual_result = avg(v2);
			Assert::AreEqual(expect_result, actual_result);
		}
	};
}
