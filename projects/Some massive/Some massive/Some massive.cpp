#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

double sum(vector <double>& v)
{
	double sum = 0;
	for (int i = 1; i < v.size(); i++) {
		sum += v[i];
	}
	return sum;
}

double avg(vector <double>& v)
{
	return sum(v) / v.size();
}

int main()	
{
	vector <double> massive = {5, 10, 20, 69, 228};
	double element;
	/*cout << "Enter elements: ";

	while (cin >> element)*/
		massive.push_back(element);

	sort(massive.begin(), massive.end());
	cout << "Max value: " << massive[massive.size() - 1] << endl;
	cout << "Min value: " << massive[1] << endl;
	cout << "Sum is: " << sum(massive) << endl;
	cout << "Average value: " << avg(massive) << endl;
	
	return 0;
}