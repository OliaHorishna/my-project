﻿#include "gtest/gtest.h"
#include "array_utils.h"
#include <vector>

using namespace std;

TEST(ArrayUtils, TestSum1)
{
	vector<double> v1{ -1, 2, 3, 24, 69 };
	auto expect_result = 97.0;
	auto actual_result = sum(v1);
	 EXPECT_EQ(expect_result, actual_result);
}

TEST(ArrayUtils, TestSum2)
{
	vector<double> v1{ 5, -10, 25, 228, 1 };
	auto expect_result = 249.0;
	auto actual_result = sum(v1);
	 EXPECT_EQ(expect_result, actual_result);
}
TEST(ArrayUtils, TestAvg1)
{
	vector<double> v2{ 5, 23, 7, 10, 41 };
	auto expect_result = 17.2;
	auto actual_result = avg(v2);
	 EXPECT_EQ(expect_result, actual_result);
}

TEST(ArrayUtils, TestAvg2)
{
	vector<double> v2{ 47, 23, 1, 69, 41 };
	auto expect_result = 36.2;
	auto actual_result = avg(v2);
	 EXPECT_EQ(expect_result, actual_result);
}