﻿// ConsoleApplication17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

int value1 = 10;
float value2 = 10.5;
int* Ipointer = &value1;
float* Fpointer = &value2;

class EmptyClass
{
};

class AbstractClass
{
public:
	virtual void funcOne() = 0;
	virtual void funcTwo() = 0;
};

class AbstractClass2
{
public:
	virtual void func1() = 0;
	virtual void func2() = 0;
	virtual void func3() = 0;
	virtual void func4() = 0;
};


class NotAbstrClass
{
public: int virtFunc(int);
};

class NotAbstrClass2
{
public: int virtFunc1(int);
		int virtFunc2(int);
		int virtFunc3(int);
};

class MixClass
{
public:
	virtual void clFunc(int);
	static int i;
};

class MixClass2
{
public:
	virtual void clFunc1(int);
	static int i;
	int j;
};

struct PointerOnStruct {
	int i;
	float f;
};

struct S_with_pointers {
	int* Ipointer;
	float* Fpointer;
};

int main()
{
	cout << "bool:\t\t" << sizeof(bool) << " bytes\n";
	cout << "char:\t\t" << sizeof(char) << " bytes\n";
	cout << "wchar_t:\t" << sizeof(wchar_t) << " bytes\n";
	cout << "char16_t:\t" << sizeof(char16_t) << " bytes\n"; // C++11 only
	cout << "char32_t:\t" << sizeof(char32_t) << " bytes\n"; // C++11 only
	cout << "short:\t\t" << sizeof(short) << " bytes\n";
	cout << "int:\t\t" << sizeof(int) << " bytes\n";
	cout << "long:\t\t" << sizeof(long) << " bytes\n";
	cout << "long long:\t" << sizeof(long long) << " bytes\n"; // C++11 only
	cout << "float:\t\t" << sizeof(float) << " bytes\n";
	cout << "double:\t\t" << sizeof(double) << " bytes\n";
	cout << "long double:\t" << sizeof(long double) << " bytes\n";
	cout << "void has no size\n\n\n";

	cout << "Size of empty class: " << sizeof(EmptyClass) << " bytes\n";
	cout << "Size of Abstract class: " << sizeof(AbstractClass) << " bytes\n";
	cout << "Size of Non Abstract class: " << sizeof(NotAbstrClass) << " bytes\n";
	cout << "Size of Mix class: " << sizeof(MixClass) << " bytes\n\n\n";

	cout << "Size of Abstract class: " << sizeof(AbstractClass2) << " bytes\n";
	cout << "Size of Non Abstract class: " << sizeof(NotAbstrClass2) << " bytes\n";
	cout << "Size of Mix class: " << sizeof(MixClass2) << " bytes\n\n\n";

	PointerOnStruct* Pptr;
	cout << "Size of Pointed Struct: " << sizeof(*Pptr) << " bytes\n";
	cout << "Size of Struct with Pointers: " << sizeof(S_with_pointers) << " bytes\n";

	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
