#include <iostream>
#include <bitset>
#include <string>
using namespace std;

int returnBit(int, int);
long long bit_on(int, int);
long long bit_off(int, int);
void bit_swap(int&, int&);
string dec_to_bin(unsigned int, string&);
long long bin_to_dec(string);
string encode_caesar(int, string);
string decode_caesar(int, string);