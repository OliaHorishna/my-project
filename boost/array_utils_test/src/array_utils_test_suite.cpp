#define BOOST_TEST_MODULE ArrayUtilsTestModule
#include <boost/test/unit_test.hpp>
#include "array_utils.h"
#include <vector>

BOOST_AUTO_TEST_SUITE( ArrayUtilsTests )

BOOST_AUTO_TEST_CASE(ShouldSuccesCalculateSum1)
{
	std::vector<double> v1{ -1, 2, 3, 24, 69 };
	auto expected = 97.0;
	auto actual = sum(v1);
	BOOST_CHECK_EQUAL(expected, actual);    
}

BOOST_AUTO_TEST_CASE(ShouldSuccesCalculateSum2)
{
	std::vector<double> v1{ 5, -10, 25, 228, 1 };
	auto expected = 249.0;
	auto actual = sum(v1);
	BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_CASE(ShouldSuccesCalculateAvg1)
{
	std::vector<double> v2{ 5, 23, 7, 10, 41 };
	auto expected = 17.2;
	auto actual = avg(v2);
	BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_CASE(ShouldSuccesCalculateAvg2)
{
	std::vector<double> v2{ 47, 23, 1, 69, 41 };
	auto expected = 36.2;
	auto actual = avg(v2);
	BOOST_CHECK_EQUAL(expected, actual);
}

BOOST_AUTO_TEST_SUITE_END()
